import { database } from "firebase-admin";

export const IsValidWord = async (
  word: string,
  wordsRef: database.Reference
) => {
  const result = await wordsRef
    .orderByValue()
    .limitToFirst(1)
    .equalTo(word)
    .once("value");
  return !result.exists();
};
