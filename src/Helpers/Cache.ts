export class Cache {
  private size: number;
  private arr: string[];
  constructor(params: { size: number }) {
    const { size } = params;
    this.size = size;
    this.arr = [];
  }

  public addElement(str: string) {
    const arr = this.arr;
    // Don't insert same word twice
    if (arr.some(item => item === str)) {
      return;
    }
    // Pop last element if we have reached cache's max size
    if (arr.length === this.size) {
      this.arr.pop();
    }

    this.arr = [str, ...arr];
  }

  public removeElement(str: string) {
    let arr = this.arr;
    this.arr = arr.filter(item => item != str);
  }

  public toArr() {
    return [...this.arr];
  }
}
