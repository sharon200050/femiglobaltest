// Auxiliary function to extract message
// Text from message object
const getMsgText = (msgObj: any): string => {
  return msgObj.text;
};

const getWordText = (wordObj: any): string => {
    return wordObj
}

// Auxiliary functions to extract parts of objects.
// For instance, message's text. That way, if an object's 
// Structure would change(for instance, message text would 
// Become some other nested field) this module's code would 
// Be the only part we should update.
export const Selectors = {
  getMsgText,
  getWordText
};
