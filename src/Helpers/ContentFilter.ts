
// Assumes that text is not an empty string
export const ContentFilter = async (params: {
  text: string;
  separator: string;
  censorSign: string;
  isValidWord: (str: string) => Promise<boolean>;
}): Promise<string> => {
  const { text, separator, isValidWord, censorSign } = params;
  const splitted = text.split(separator);
  let newTextSplitted = [];

  // It could be better to replace this loop
  // With map function, but async functions inside
  // Array.map aren't supported
  for (const [index, word] of splitted.entries()) {
    // Empty string means two seprators
    // Consecutive separators. For instance, "a  b"
    // With " " as delimiter Would be ["a", "", "b"]
    if (word.length === 0) {
      // Don't append seprator to last element
      if (index < splitted.length - 1) {
        newTextSplitted.push(separator);
      }
      continue;
    }

    const nextWord = (await isValidWord(word)) ? word : censorSign;
    newTextSplitted.push(nextWord);
    // Don't append seprator to last element
    if (index < splitted.length - 1) {
      newTextSplitted.push(separator);
    }
  }

  return newTextSplitted.join("");
};
