import * as admin from "firebase-admin";
import { RefPaths } from "./Constants/RefPaths";
import { Text } from "./Constants/Text";
import { Config } from "./Constants/Config";
import { Selectors } from "./Helpers/Selectors";
import { ContentFilter } from "./Helpers/ContentFilter";
import { IsValidWord } from "./Helpers/IsValidWord";
import { Cache } from "./Helpers/Cache";

const serviceAccount = require("../ServiceKey.json");

// Initialize cache for words 
// That should be censored
const cache = new Cache({ size: 3 });

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: Config.databaseUrl
});

const db = admin.database();
const messagesRef = db.ref(RefPaths.messages);
const wordsRef = db.ref(RefPaths.words);

const logger = console.log;

messagesRef.on("child_added", async (snapshot, a) => {
  if (!snapshot) {
    return;
  }

  const { separator, censorSign } = Text;
  const text = Selectors.getMsgText(snapshot.val());
  //Censor message's text and update
  snapshot.ref.update({
    text: await ContentFilter({
      text,
      separator,
      censorSign,
      isValidWord: (str: string) => {
        // If the word is in cache than it
        // Should be censored
        if (cache.toArr().some(item => item === str)) {
          return Promise.resolve(false);
        }

        return IsValidWord(str, wordsRef);
      }
    })
  });
  // logger("New message received: " + text);
});

wordsRef.on("child_added", (snapshot, a) => {
  if (!snapshot) {
    return;
  }

  const word = Selectors.getWordText(snapshot.val());

  // Update cache
  cache.addElement(word);
  logger("New word was added to the black list: " + word);
});

wordsRef.on("child_removed", (snapshot, a) => {
  if (!snapshot) {
    return;
  }

  const word = Selectors.getWordText(snapshot.val());

  // Update cache
  cache.removeElement(word);
  logger("A word was removed from the black list: " + word);
});
