const separator = " ";
const censorSign = "*";

export const Text = {
    separator,
    censorSign
}